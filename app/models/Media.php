<?php

class Media extends Eloquent {
    
    protected $fillable = array('title','description','filepath');

    public static $rules = array(
    	'title'=>'required',
		'description'=>'required'
	);

	public static function validate($data){
		return Validator::make($data, static::$rules);
	}
    
}
