<?php

class Challenge extends Eloquent {
    
    protected $fillable = array('title','description','latitude','longitude','owner');

    public static $rules = array(
        'title'=>'required',
		'description'=>'required',
		'latitude'=>'required',
		'longitude'=>'required',
        'owner'=>'required|numeric'
	);

	public static function validate($data){
		return Validator::make($data, static::$rules);
	}
    
}