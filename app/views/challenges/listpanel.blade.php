<div class="panel">
	<h2>Challenges</h2>
	<ul class="challenges">
	@foreach($challenges as $c)
		<li>{{ HTML::linkRoute('challengeSingle',$c->title,array('id'=>$c->id)) }}</li>
	@endforeach
	</ul>
</div>