@if(Auth::check())
<div class="panel">

    <h2>Create Media</h2>

	{{ Form::open(array('url' => 'media/create', 'files'=> true)) }}

	<ul class="errors">
	@foreach($errors->all() as $message)
		<li>{{ $message }}</li>
	@endforeach
	</ul>
	
	<table>
	<tr>
		<td>{{ Form::label('title','Title:') }}</td>
		<td>{{ Form::text('title') }}</td>
	</tr>
	<tr>
		<td>{{ Form::label('description','Description:') }}</td>
		<td>{{ Form::text('description') }}</td>
	</tr>
	<tr>

		<td>{{ Form::label('file','File:') }}</td>
		<td>{{ Form::file('file') }}</td>
	</tr>
	</table>
    
	{{ Form::submit('Create', array('class'=>'button')) }}

	{{ Form::close() }}

</div>
@endif