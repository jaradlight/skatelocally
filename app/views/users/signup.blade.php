@if(Auth::guest())
<div class="panel">
	<h2>Sign Up</h2>

	{{ Form::open(array('url' => 'signup')) }}

	<ul class="errors">
	@foreach($errors->all() as $message)
		<li>{{ $message }}</li>
	@endforeach
	</ul>
	
	<table>
	<tr>
		<td>{{ Form::label('username','Username:') }}</td>
		<td>{{ Form::text('username') }}</td>
	</tr>
	<tr>
		<td>{{ Form::label('email','Email:') }}</td>
		<td>{{ Form::email('email') }}</td>
	</tr>
	<tr>
		<td>{{ Form::label('password','Password:') }}</td>
		<td>{{ Form::password('password') }}</td>
	</tr>
	</table>

	{{ Form::submit('Sign Up', array('class'=>'button')) }}

	{{ Form::close() }}

</div>
@elseif

<h1>You are signed in.</h1>

@endif