<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the Closure to execute when that URI is requested.
|
*/

Route::get('/', function(){
	$challenges = Challenge::all();

	return View::make('landing')
		->with('challenges', $challenges);
});

Route::get('users', function(){
	return View::make('users.index');
});

Route::post('signup', array('as'=>'signup','uses'=>'UserController@signup'));

Route::post('login', array('as'=>'login','uses'=>'UserController@login'));

Route::get('logout', array('as'=>'logout','uses'=>'UserController@logout'));


Route::get('login', function(){ return "You are trying to log in."; });
Route::post('logout', function(){ return "You are trying to post to log out."; });
Route::get('signup', function(){ return "You are trying to sign up."; });

Route::post('challenge/create', array('as'=>'createChallenge','uses'=>'ChallengeController@create'));

Route::get('challenge/{id}', array('as'=>'challengeSingle','uses'=>'ChallengeController@single'));

Route::post('media/create', array('as'=>'createMedia','uses'=>'MediaController@create'));
