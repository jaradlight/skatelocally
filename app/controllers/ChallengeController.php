<?php

class ChallengeController extends \BaseController {

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{
		//
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create(){
    	$validation = Challenge::validate(Input::all());

		if($validation->fails()){
			return Redirect::to('/')
				->withErrors($validation)
				->with('message','Challenge creation failed.')
				->withInput();
		} else {

			Challenge::create(array(
				'title'=>Input::get('title'),
				'description'=>Input::get('description'),
				'latitude'=>Input::get('latitude'),
				'longitude'=>Input::get('longitude'),
                'owner'=>Auth::user()->id
			));

			return Redirect::to('/')
				->with('message', 'Challenge created.');
            
            return "Success!";
		}
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store()
	{
		//
	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		//
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		//
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{
		//
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		//
	}

	public function single($id){
		return View::make('challenges.single')
			->with('challenge',Challenge::find($id));
	}

}